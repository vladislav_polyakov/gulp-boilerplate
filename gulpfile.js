'use strict';

const { src, dest, series, parallel, watch } = require('gulp');
const del = require('del');
const gulpif = require('gulp-if');

const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const concatCss = require('gulp-concat-css');
const miniCss = require('gulp-mini-css');

const uglify = require('gulp-uglify-es').default;

const imageMin = require('gulp-imagemin');

const rename = require('gulp-rename');

const browserSync  = require('browser-sync').create();
const isProd = process.argv.includes('--production');

function clean() {
  return del(['dist']);
}

function html() {
  return src('./src/*.html')
    .pipe(dest('./dist/'))
    .pipe(gulpif(!isProd, browserSync.reload({ stream: true })));
}

async function styles() {
  return src('./src/styles/**/*scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulpif(isProd, autoprefixer({
      overrideBrowserslist: ['last 10 versions'],
    })))
    .pipe(concatCss('bundle.css'))
    .pipe(miniCss({ext:'.min.css'}))
    .pipe(dest('./dist/styles'))
    .pipe(gulpif(!isProd, browserSync.stream()));
}

async function scripts() {
  return src('./src/scripts/*.js')
    .pipe(uglify())
    .pipe(rename({ suffix: '.min' }))
    .pipe(dest('./dist/scripts/'))
    .pipe(browserSync.reload({ stream: true }));
}

async function images() {
  return src('./src/images/**/*')
    .pipe(gulpif(isProd, imageMin([
      imageMin.mozjpeg({ quality: 75, progressive: true }),
      imageMin.optipng({ optimizationLevel: 5 }),
    ])))
    .pipe(dest('./dist/images/'));
}

async function files() {
  return src('./src/static/**/*')
    .pipe(dest('./dist/static/'));
}

function watchAll() {
  watch('./src/styles/**/*scss', styles);
  watch('./src/scripts/**/*.js', scripts);
  watch('./src/*.html', html);
}

function liveReload() {
  browserSync.init({
    server: {
      baseDir: 'dist',
    },
    notify: false,
  });
}

if (isProd) {
  exports.default = series(clean, parallel(styles, scripts, images, html, files));
} else {
  exports.default = series(clean, parallel(styles, scripts, images, html, files), parallel(liveReload, watchAll));
}
